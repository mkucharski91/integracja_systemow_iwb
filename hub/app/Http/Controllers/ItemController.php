<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;

class ItemController extends Controller
{
    //
    public function index(){
        $client = new Client();
        $realEstatesResponse = $client->request('GET', 'http://localhost/provider1/realestates');
        $apartmentsResponse = $client->request('GET', 'http://localhost/provider2/apartments');

        $realEstates = json_decode($realEstatesResponse->getBody(), true);
        $apartments = json_decode($apartmentsResponse->getBody(), true);
        $items = array_merge($this->convertrealEstates($realEstates), $this->convertApartments($apartments));
        
        return response()->json($items);
        return $items;

    }
    public function show($rec_id){
        $client = new Client();
        $realEstatesResponse = $client->request('GET', "http://localhost/provider1/realestates/{$rec_id}");
        $apartmentsResponse = $client->request('GET', "http://localhost/provider2/apartments/{$rec_id}");
        $realEstates = json_decode($realEstatesResponse->getBody(), true);
        $apartments = json_decode($apartmentsResponse->getBody(), true);
        $item_re = [
                'item_id' => $realEstates['id'],
                'cena' => $realEstates['cena'],
                'lokalizacja' => $realEstates['lokalizacja'],
                'powierzchnia' => $realEstates['powierzchnia'],
                'rodzaj' => $realEstates['rodzaj'],
                'dostawca' => 'realEstates'
            ];
        $item_ap = [
                'item_id' => $apartments['id'],
                'cena' => $apartments['wartosc'],
                'lokalizacja' => $apartments['lokalizacja'],
                'powierzchnia' => $apartments['wielkosc'],
                'rodzaj' => $apartments['typ'],
                'dostawaca' => 'apartments'
            ];
        $items = array_merge($item_re, $item_ap);
        shuffle($items);
        return response()->json($items);
        return $items;
    }

    private function convertrealEstates($realEstates){
        $result = [];
        foreach ($realEstates as $realEstate){
            $item = [
                'item_id' => $realEstate['id'],
                'cena' => $realEstate['cena'],
                'lokalizacja' => $realEstate['lokalizacja'],
                'powierzchnia' => $realEstate['powierzchnia'],
                'rodzaj' => $realEstate['rodzaj'],
                'dostawca' => 'realEstates'
            ];
            array_push($result, $item);
        }
        return $result;
    }
    private function convertApartments($apartments){
        $result = [];
        foreach ($apartments as $apartment){
            $item = [
                'item_id' => $apartment['id'],
                'cena' => $apartment['wartosc'],
                'lokalizacja' => $apartment['lokalizacja'],
                'powierzchnia' => $apartment['wielkosc'],
                'rodzaj' => $apartment['typ'],
                'dostawaca' => 'apartments'
            ];
            array_push($result, $item);
        }
        return $result;
    }
}
