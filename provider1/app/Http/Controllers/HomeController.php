<?php

namespace App\Http\Controllers;
use App\Realestates;

class HomeController extends Controller
{
    //
    public function index(){
        return view('welcome');
    }

    public function GetData(){
    	return response()->json(Realestates::all());	
    	
    }	
    
    public function GetDataById($id){
    	return response()->json(Realestates::find($id));	
    }
        	
}
