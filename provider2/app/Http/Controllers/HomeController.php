<?php

namespace App\Http\Controllers;
use App\Apartments;

class HomeController extends Controller
{
    //
    public function index(){
        return view('welcome');
    }

    public function GetData(){
    	return response()->json(Apartments::all());	
    	
    }	
    
    public function GetDataById($id){
    	return response()->json(Apartments::find($id));	
    }
        	
}
